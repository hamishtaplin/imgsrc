#  ____
# / ___|  ___  __ _ _   _  ___ _ __   ___  ___
# \___ \ / _ \/ _` | | | |/ _ \ '_ \ / __|/ _ \
#  ___) |  __/ (_| | |_| |  __/ | | | (__|  __/
# |____/ \___|\__, |\__,_|\___|_| |_|\___|\___|
#                |_|

"use strict"

utils = Namespace('SEQ.utils')
mego = Namespace("SEQ.mego")

class App
  #init function happens as soon as javascript is loaded
  constructor: ->
    document.addEventListener "DOMContentLoaded", @onDocReady
    window.addEventListener("load", @onWindowLoad)

  onWindowLoad: =>
    document.body.style.opacity = 1

  # executes when document is ready
  onDocReady: =>
    @loading = false

    @initFields()

    @initColorPickers()
    @initRandomColorBtns()

    @initScrollyThing()
    @initPreviewImage()

    document.getElementById("preview-btn").addEventListener("click", @updatePreview)

    for input in document.querySelectorAll("input, select")
      input.addEventListener("change", @updateValues)
      input.addEventListener("keyup", @onKeyUp)

  initFields: ->
    @widthText = document.getElementById("width")
    @heightText = document.getElementById("height")
    @getSrc = document.getElementById("get")
    @font = document.getElementById("font")
    @fontColourText = document.getElementById("font-colour")
    @backgroundColourText = document.getElementById("background-colour")
    @displayText = document.getElementById("text")
    @flickrTags = document.getElementById("flickr-tags")
    @format = document.getElementById("format-field")

  initScrollyThing: () ->
    @optionsContainer = document.getElementById("options-container")
    offset = @optionsContainer.offsetTop + 38
    document.addEventListener "scroll", (e) =>
      if window.scrollY >= offset
        document.body.className = "fixed-toolbar"
      else
        document.body.className = ""

  getFieldValues: () ->
    @values =
      width: @widthText.value.replace(/px/, '')
      height: @heightText.value.replace(/px/, '')
      font: @font.options[e=@font.selectedIndex].value || null
      textColour: @fontColourText.value.replace(/#/, '')
      backgroundColour: @backgroundColourText.value.replace(/#/, '')
      displayText: @displayText.value || null
      flickrTags: @flickrTags.value || null

  initPreviewImage: ->
    @previewDiv = document.getElementById("preview-image")
    @loadNewImg()

  updatePreview: () =>
    return if @loading

    $(@previewDiv).transition
      opacity: 0
      duration: 300
      complete: @loadNewImg

  updateValues: =>
    @url = @getUrl()
    @getSrc.value = @url

  onKeyUp: (e) =>
    if e.keyCode is 13
      @updatePreview()
    else
      return if e.target.id is "get"
      @updateValues()

  getUrl: ->
    @getFieldValues()
    @url = "http://imgsrc.me/#{@values.width}x#{@values.height}/"

    if @values.backgroundColour.length > 0
      @url += "#{@values.backgroundColour}/"
    if @values.textColour.length > 0
      @url += "#{@values.textColour}"
    if @values.displayText? and @values.displayText.length > 0
      @url += "/#{@values.displayText}?showDimensions=0&"
    else
      @url += "?"
    if @values.font? and @values.font != "helvetica"
      @url += "font=#{@values.font}&"
    if @values.flickrTags? and @values.flickrTags.length > 0
      @url += "flickrTags=#{@values.flickrTags}"
    # remove redundant ampersands from end
    if @url.substr(@url.length+1) is "&"
      @url = @url.substring(0, @url.length)
    return @url

  loadNewImg: =>
    @updateValues()

    @img = new Image()
    @img.addEventListener("load", @onImageLoaded)
    @img.src = @url

  onImageLoaded: (e) =>
    oldImg = @previewDiv.querySelectorAll("img")[0]
    shadow = document.getElementById("preview-shadow")
    shadowHeight = shadow.clientHeight
    @previewDiv.style.height = oldImg.clientHeight + shadowHeight
    @previewDiv.replaceChild(@img, oldImg)

    shadow.setAttribute("height", parseInt(shadowHeight))
    shadow.setAttribute("width", @img.clientWidth + (@img.clientWidth/6))

    if @previewDiv.clientHeight != @img.clientHeight
      $(@previewDiv).transition
        height: @img.clientHeight
        duration: 200
        complete: @onPreviewHeightAnimComplete
    else
      @onPreviewHeightAnimComplete()

  onPreviewHeightAnimComplete: =>
    $(@previewDiv).transition
      opacity: 1
      duration: 500
      complete: =>
        @loading = false

  initRandomColorBtns: ->
    document.getElementById("random-font-colour-btn").addEventListener "click", =>
      @animateTextfieldValues(@fontColourText)
    document.getElementById("random-background-colour-btn").addEventListener "click", =>
      @animateTextfieldValues(@backgroundColourText)

  animateTextfieldValues: (textField) ->
    num = 20
    i = 0
    timer = setInterval( =>
      return clearInterval(timer) if num is i
      textField.value = @getRandomColor()
      i++
      @updateValues()
    , 40)

  getRandomColor: ->
    return '#'+Math.floor(Math.random()*16777215).toString(16);

  initColorPickers: =>
    opts =
      display: false
      size: 200
      autoclose: true
      drag: false
      margin: 0
      hueWidth: 40

    fontColourPickerOpts =
      color: @fontColourText.value || @fontColourText.placeholder || "#000000"
      callback: (rgba, state, type) =>
        @fontColourText.value = "#" + Color.Space(rgba, "RGB>HEX>STRING")

    backgroundColourPickerOpts =
      color: @backgroundColourText.value || @backgroundColourText.placeholder || "#ffffff"
      callback: (rgba, state, type) =>
        @backgroundColourText.value = "#" + Color.Space(rgba, "RGB>HEX>STRING")

    $.extend fontColourPickerOpts, opts
    $.extend backgroundColourPickerOpts, opts

    @fontColourPicker = new Color.Picker fontColourPickerOpts
    @backgroundColourPicker = new Color.Picker backgroundColourPickerOpts

    document.getElementById("font-color-picker-btn").addEventListener "click", this.onFontColorPickerClick
    document.getElementById("background-color-picker-btn").addEventListener "click", this.onBackgroundColorPickerClick

  onBackgroundColorPickerClick: (e) =>
    @backgroundColourPicker.element.style.top = (@backgroundColourText.offsetTop - 230) + "px"
    @backgroundColourPicker.element.style.left = (@backgroundColourText.offsetLeft + 110) + "px"
    @backgroundColourPicker.toggle();

  onFontColorPickerClick: (e) =>
    @fontColourPicker.element.style.top = (@fontColourText.offsetTop - 230) + "px"
    @fontColourPicker.element.style.left = (@fontColourText.offsetLeft + 110) + "px"
    @fontColourPicker.toggle();
    $(document.getElementById("font-color-picker-btn")).addClass("checked")


mego.app = new App()

